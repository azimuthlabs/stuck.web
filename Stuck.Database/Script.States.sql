﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('New South Wales','NSW',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Queensland','QLD',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('South Australia','SA',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Tasmania','TAS',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Victoria','VIC',1)

INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Western Australia','WA',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Australian Capital Territory','ACT',1)


INSERT INTO [dbo].[States] 
(State,Code,IsActive)
VALUES
('Northern Territory','NT',1)