﻿CREATE TABLE [dbo].[States]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[State] Varchar(50) NOT NULL,
	[Code] Varchar(5) NOT NULL,
	[IsActive] Bit NOT NULL
)
