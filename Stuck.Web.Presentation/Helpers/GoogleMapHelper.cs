﻿using BasePresentation.Lookup;
using GoogleMaps.LocationServices;
using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stuck.Web.Presentation.Helpers
{
    public class GoogleMapHelper
    {
        public static LatLongModel GetLatLong(string Address)
        {
            LatLongModel model = new LatLongModel();
            var locationService = new GoogleLocationService(Generic_Contsants.GoogleMapKey);
            var point = locationService.GetLatLongFromAddress(Address);

            model.latitude = point.Latitude;
            model.longitude = point.Longitude;
            return model;
        }
    }
}