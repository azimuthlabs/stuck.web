﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using Stuck.API.Infrastructure;
using Stuck.API.Services;
using Stuck.Domain;
using Stuck.Services;
using Stuck.Services.Interfaces;

namespace Stuck.Web.Presentation.App_Start
{
    public class ContainerConfig
    {
        public static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(typeof(ContainerConfig).Assembly);
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<BaseStuckWebAPI>().As<IBaseStuckWebAPI>().InstancePerRequest();
            builder.RegisterType<StuckWebServices>().As<IStuckWebServices>().InstancePerRequest();

            builder.RegisterType<BaseRepository<User>>().As<IBaseRepository<User>>().InstancePerRequest();
            builder.RegisterType<BaseRepository<State>>().As<IBaseRepository<State>>().InstancePerRequest();
            builder.RegisterType<BaseRepository<BlockedPostCode>>().As<IBaseRepository<BlockedPostCode>>();

            return builder.Build();
        }
    }
}