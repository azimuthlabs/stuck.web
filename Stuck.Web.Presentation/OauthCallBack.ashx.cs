﻿using System;
using System.Threading.Tasks;
using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Auth0.AspNet;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Services;
using System.Web;

namespace Stuck.Web.Presentation
{
    /// <summary>
    /// Summary description for OauthCallBack
    /// </summary>
    public class OauthCallBack : HttpTaskAsyncHandler, System.Web.SessionState.IRequiresSessionState
    {

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            if (context.Request.QueryString != null && context.Request.QueryString.Count > 0)
            {
                //    AuthenticationApiClient client = new AuthenticationApiClient(
                //        new Uri(string.Format("https://{0}", ConfigurationManager.AppSettings["auth0:Domain"])));

                //    var token = await client.GetTokenAsync(new AuthorizationCodeTokenRequest
                //    {
                //        ClientId = ConfigurationManager.AppSettings["auth0:ClientId"],
                //        ClientSecret = ConfigurationManager.AppSettings["auth0:ClientSecret"],
                //        Code = context.Request.QueryString["code"].ToString(),
                //        RedirectUri = context.Request.Url.ToString()
                //    });

                //    // get access token in session
                //    context.Session["access_token"] = token.IdToken;

                //    var profile = await client.GetUserInfoAsync(token.AccessToken);

                //    var user = new List<KeyValuePair<string, object>>
                //{
                //    new KeyValuePair<string, object>("name", profile.FullName ?? profile.PreferredUsername ?? profile.Email),
                //    new KeyValuePair<string, object>("email", profile.Email),
                //    new KeyValuePair<string, object>("family_name", profile.LastName),
                //    new KeyValuePair<string, object>("given_name", profile.FirstName),
                //    new KeyValuePair<string, object>("nickname", profile.NickName),
                //    new KeyValuePair<string, object>("picture", profile.Picture),
                //    new KeyValuePair<string, object>("user_id", profile.UserId),
                //    new KeyValuePair<string, object>("id_token", token.IdToken),
                //    new KeyValuePair<string, object>("access_token", token.AccessToken),
                //    new KeyValuePair<string, object>("refresh_token", token.RefreshToken)
                //};

                //    FederatedAuthentication.SessionAuthenticationModule.CreateSessionCookie(user);
                //    System.Web.Security.FormsAuthentication.SetAuthCookie(profile.FullName ?? profile.PreferredUsername ?? profile.Email, false);

                //    var state = context.Request.QueryString["state"];
                //    if (state != null)
                //    {
                //        var stateValues = HttpUtility.ParseQueryString(context.Request.QueryString["state"]);
                //        var redirectUrl = stateValues["ru"];

                //        // check for open redirection
                //        if (redirectUrl != null && IsLocalUrl(redirectUrl))
                //        {
                //            context.Response.Redirect(redirectUrl, true);
                //        }
                //    }

                context.Response.Redirect("/Account/Token?code=" + context.Request.QueryString["code"].ToString(), true);

            }
            else
            {
                context.Response.Redirect("/Account/GetAuthTokenDetails", false);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private bool IsLocalUrl(string url)
        {
            return !String.IsNullOrEmpty(url)
                && url.StartsWith("/")
                && !url.StartsWith("//")
                && !url.StartsWith("/\\");
        }
    }
}