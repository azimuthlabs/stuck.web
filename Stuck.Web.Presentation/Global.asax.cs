﻿using Autofac.Integration.Mvc;
using Stuck.Web.Presentation.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Stuck.Web.Presentation
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //SJ container for dependency injection
            var container = ContainerConfig.CreateContainer();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }


    }
}
