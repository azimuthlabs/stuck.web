﻿var map;
var geocoder;
var markers = new Array();

function GetCurrentLocation(showConfirmLocation) {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            //debugger;
            DrawMarkerOnMap(position.coords.latitude, position.coords.longitude);
            getAddressFromLatLang(position.coords.latitude, position.coords.longitude)
            if (showConfirmLocation) {
                SetLocationMarker(position.coords.latitude, position.coords.longitude);
            }
        });
    } else {
        alert("Browser doesn't support geolocation!");
    }
}

function getDestinationAddressFromLatLang(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ 'latLng': latLng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                $("#Destination").val(results[0].formatted_address);
            }
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function getAddressFromLatLang(lat, lng) {
    var geocoder = new google.maps.Geocoder();
    var latLng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({ 'latLng': latLng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                var restricted = false;
                if (results[0] != undefined && results[0].address_components != undefined && results[0].address_components.length > 0)
                {
                    if (servicesRestricedAreas != undefined)
                    {
                        var postcodes = servicesRestricedAreas.split(",");
                        $.each(postcodes, function (i) {
                            if (postcodes[i] == results[0].address_components[results[0].address_components.length - 1].short_name) {
                                ShowServicesNotAvailable();
                                restricted = true;
                            }
                        });
                    }
                }

                if (!restricted)
                {
                    $("#Address").val(results[0].formatted_address);
                }
                else
                {
                    $("#Address").val('');
                }
            }
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
    // console.log("Entering getAddressFromLatLang()");
}

function DrawMap() {
    //var mapCenter = new google.maps.LatLng(20.5937, 78.9629); //Google map Coordinates
    var mapCenter = new google.maps.LatLng(-35.473469, 149.012375);
    map = new google.maps.Map($("#divMap")[0], {
        center: mapCenter,
        zoom: 5
    });
}

function GetDestinationByAddress(address) {
    geocoder = new google.maps.Geocoder;
    geocoder.geocode({ address: address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var postion = results[0].geometry.location;
            DrawMarkerByCordinates(postion.lat(), postion.lng());
        }
        else {
        }
    });
}

function GetAddressByPosition(latitude, logntitude) {
    var address = "";
    geocoder = new google.maps.Geocoder;
    var latlng = { lat: parseFloat(latitude), lng: parseFloat(logntitude) };
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $("#Address").val(results[0].formatted_address);
                console.log(results[0]);
            } else {
                address = 'Geocoder No results found for latitude and logntitude';
            }
        } else {
            address = 'Geocoder failed due to: ' + status;
        }
    });
    return address;
}

function GetPostionByAddress(address) {
    geocoder = new google.maps.Geocoder;
    geocoder.geocode({ address: address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var postion = results[0].geometry.location;
            DrawMarkerOnMap(postion.lat(), postion.lng());
            var showConfirmLocation = $("#showConfirmLocation").val();
            if (showConfirmLocation == "show") {
                SetLocationMarker(postion.lat(), postion.lng());
            }
        }
        else {
            // console.log(status);
        }
    });
}

function PlugAddressSuggestion() {
    var options = {
        // types: ['(cities)'],
        componentRestrictions: { country: 'au' }
    };

    // debugger;
    // var autocomplete = new google.maps.places.Autocomplete($("#Address")[0], options);

    // google.maps.event.addListener(autocomplete, 'place_changed', AddresChanged);

    var places = new google.maps.places.Autocomplete($("#Address")[0], options);
    google.maps.event.addListener(places, 'place_changed', function () {
        var place = places.getPlace();
        var address = place.formatted_address;
        var value = address.split(",");
        count = value.length;
        country = value[count - 1];
        state = value[count - 2];
        city = value[count - 3];
        var z = state.split(" ");
        var i = z.length;
        var restricted = false;
        if (i > 2) {
            if (z != undefined && z.length > 0) {

                if (servicesRestricedAreas != undefined) {
                    var postcodes = servicesRestricedAreas.split(",");
                    $.each(postcodes, function (i) {
                        if (postcodes[i] == z[z.length - 1]) {
                            ShowServicesNotAvailable();
                            restricted = true;
                        }
                    });
                }
            }
        }
        if (!restricted)
            GetPostionByAddress($("#Address").val());
    });

}



function PlugDesintationSuggestion() {
    var options =
        {
            // types: ['(cities)'],
            componentRestrictions: { country: 'au' }
        };

    // debugger;
    var autocomplete = new google.maps.places.Autocomplete($("#Destination")[0], options);

    google.maps.event.addListener(autocomplete, 'place_changed', DestinationChanged);
}

function DestinationChanged() {
    GetDestinationByAddress($("#Destination").val())
}

function AddresChanged() {
    // GetPostionByAddress($("#Address").val());
}

function DrawMarkerOnMap(latitude, longitude) {

    var showConfirmLocation = $("#showConfirmLocation").val();
    //if (showConfirmLocation == "show") {
    if (markers != undefined) {
        clearMarkers();
    }
    //}

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        animation: google.maps.Animation.DROP,
        map: map,
        // draggable: true,
    });

    if (showConfirmLocation == "show") {
        google.maps.event.addListener(map, "click", function (e) {
            //lat and lng is available in e object
            DrawMarkerOnMap(e.latLng.lat(), e.latLng.lng());
            SetLocationMarker(e.latLng.lat(), e.latLng.lng());
            getAddressFromLatLang(e.latLng.lat(), e.latLng.lng());
        });
        google.maps.event.addListener(marker, 'dragend', function () {
            //lat and lng is available in e object
            DrawMarkerOnMap(marker.latLng.lat(), marker.latLng.lng());
            SetLocationMarker(marker.latLng.lat(), marker.latLng.lng());
            getAddressFromLatLang(marker.latLng.lat(), marker.latLng.lng());
        });
    }

    markers.push(marker);

    // set marker higihlight 
    map.setZoom(15);
    map.panTo(marker.getPosition());

    // get services based on location
    GetServicesByLocation(latitude, longitude);
}

function setMapOnAll(map) {
    if (markers != undefined && markers.length > 0) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
            markers[i] = null;
        }
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
    markers = new Array();
}

function DrawMarkerByCordinates(latitude, longitude) {
    var confirmBookingMove = $("#ConfirmBookingMove").val();
    var comarker;

    if (confirmBookingMove == "show") {
        if (markers != undefined) {
            clearMarkers();
        }

        comarker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            animation: google.maps.Animation.DROP,
            map: map,
            draggable: true,
        });

        google.maps.event.addListener(map, "click", function (e) {
            //lat and lng is available in e object
            DrawMarkerByCordinates(e.latLng.lat(), e.latLng.lng());
            getDestinationAddressFromLatLang(e.latLng.lat(), e.latLng.lng());
        });
        google.maps.event.addListener(comarker, 'dragend', function (e) {
            //lat and lng is available in e object
            DrawMarkerByCordinates(e.latLng.lat(), e.latLng.lng());
            getDestinationAddressFromLatLang(e.latLng.lat(), e.latLng.lng());
        });
        markers.push(comarker);
    }
    else {
        comarker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            animation: google.maps.Animation.DROP,
            map: map,
            // draggable: true,
        });
    }
    // set marker higihlight 
    map.setZoom(15);
    map.panTo(comarker.getPosition());
}

function SetLocationMarker(latitude, longitude) {
    var locationmarker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        animation: google.maps.Animation.DROP,
        map: map,
        draggable: true
    });

    // InfoWindow content
    var content = '<div id="iw-container">' +
        '<a href="javascript:void()" onclick="confirmbookingrequest()" style="text-decoration:none;"><div class="iw-title">CONFIRM LOCATION</div></a>' +
        //'<div class="iw-content">' +
        ///'<div class="iw-subTitle">History</div>' +
        //'<img src="http://maps.marnoto.com/en/5wayscustomizeinfowindow/images/vistalegre.jpg" alt="Porcelain Factory of Vista Alegre" height="115" width="83">' +
        //'<p>Founded in 1824, the Porcelain Factory of Vista Alegre was the first industrial unit dedicated to porcelain production in Portugal. For the foundation and success of this risky industrial development was crucial the spirit of persistence of its founder, José Ferreira Pinto Basto. Leading figure in Portuguese society of the nineteenth century farm owner, daring dealer, wisely incorporated the liberal ideas of the century, having become "the first example of free enterprise" in Portugal.</p>' +
        //'<div class="iw-subTitle">Contacts</div>' +
        //'<p>VISTA ALEGRE ATLANTIS, SA<br>3830-292 Ílhavo - Portugal<br>' +
        //'<br>Phone. +351 234 320 600<br>e-mail: geral@vaa.pt<br>www: www.myvistaalegre.com</p>' +
        //'</div>' +
        //'<div class="iw-bottom-gradient"></div>' +
        '</div>';

    // A new Info Window is created and set content
    var infowindow = new google.maps.InfoWindow({
        content: content,
        // Assign a maximum value for the width of the infowindow allows
        // greater control over the various content elements
        maxWidth: 300
    });

    // This event expects a click on a marker
    // When this event is fired the Info Window is opened.
    google.maps.event.addListener(locationmarker, 'click', function () {
        infowindow.open(map, locationmarker);
    });

    // Event that closes the Info Window with a click on the map
    google.maps.event.addListener(map, 'click', function () {
        infowindow.close();
    });

    google.maps.event.addListener(locationmarker, 'dragend', function () {
        //lat and lng is available in e object
        DrawMarkerOnMap(locationmarker.getPosition().lat(), locationmarker.getPosition().lng());
        SetLocationMarker(locationmarker.getPosition().lat(), locationmarker.getPosition().lng());
        getAddressFromLatLang(locationmarker.getPosition().lat(), locationmarker.getPosition().lng());
    });

    // *
    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    // *
    google.maps.event.addListener(infowindow, 'domready', function () {

        // Reference to the DIV that wraps the bottom of infowindow
        var iwOuter = $('.gm-style-iw');

        /* Since this div is in a position prior to .gm-div style-iw.
         * We use jQuery and create a iwBackground variable,
         * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
        */
        var iwBackground = iwOuter.prev();

        // Removes background shadow DIV
        iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

        // Removes white background DIV
        iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

        // Moves the infowindow 115px to the right.
        iwOuter.parent().parent().css({ left: '0px' });

        // Moves the shadow of the arrow 76px to the left margin.
        iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s }); //'left: 76px !important;'

        // Moves the arrow 76px to the left margin.
        iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s });

        // Changes the desired tail shadow color.
        iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index': '1' });

        // Reference to the div that groups the close button elements.
        var iwCloseBtn = iwOuter.next();

        // Apply the desired effect to the close button
        iwCloseBtn.css({ opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9' });

        // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
        if ($('.iw-content').height() < 140) {
            $('.iw-bottom-gradient').css({ display: 'block' });
        }

        // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function () {
            $(this).css({ opacity: '1' });
        });
    });

    infowindow.open(map, locationmarker);

    markers.push(locationmarker);

    // set marker higihlight 
    map.setZoom(15);
    map.panTo(marker.getPosition());

}