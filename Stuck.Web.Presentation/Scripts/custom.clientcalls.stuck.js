﻿
// generic function
function GenericAjaxCall(type, apiurl, parameters, successCallback) {
    $.ajax({
        type: type,
        url: apiurl,
        data: JSON.stringify(parameters),
        contentType: 'application/json;',
        dataType: 'json',
        success: successCallback,
        error: function (xhr, textStatus, errorThrown) {
            console.log('error');
        }
    });
}

function GetServicesByLocation(latitude, longtitude) {
    GenericAjaxCall('POST', '/Landing/GetServicesForLocation', { latitude: latitude, longtitude: longtitude }, onSuccess);
}

function onSuccess(services) {
    // draw div with services
    var divstring = "<div class='box-outer'>";
    var Address = $("#Address").val();
    for (var i = 0; i < services.length; i++) {
        if (services[i].active == true) {
            // debugger;
            var price = "";
            if (services[i].fixedPricingDetails != null) {
                price = "$ " + services[i].fixedPricingDetails.indicativeCost;
            }
            else {
                price = "$ " + services[i].variablePricingDetails.basePrice; // tow issue
            }
            divstring = divstring + "<a href='javascript:void(0)' onclick=" + "ConfirmBookingSubmit('" + services[i].serviceId + "')><div class='box'>" + services[i].serviceDescription + "<br/><img width='70px' height='70px' src='" + services[i].serviceIconUrl + "' /><br/> " + price + "</div></a>";
        }
        else {
            divstring = divstring + "<a href='javascript:void(0)' onclick=" + "ShowBookingServiceNotAvailableDetails('" + services[i].serviceId + "','" + services[i].serviceIconUrl + "')><div class='box'>" + services[i].serviceDescription + "<br/><img width='70px' height='70px' src='" + services[i].serviceIconUrl + "' /></div></a>";
        }
    }
    divstring = divstring + "</div>";

    $("#divStuckServices").html(divstring);
}
