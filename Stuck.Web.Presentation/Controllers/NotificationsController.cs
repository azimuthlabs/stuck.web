﻿using Stuck.API.Services;
using Stuck.BasePresentation.Models;
using Stuck.BasePresentation.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stuck.Web.Presentation.Helpers;
using BasePresentation.Lookup;

namespace Stuck.Web.Presentation.Controllers
{
    public class NotificationsController : BaseController
    {
        readonly IStuckWebServices _stuckWebService;

        public NotificationsController(IStuckWebServices stuckWebService)
        {
            _stuckWebService = stuckWebService;
        }

        [HttpGet]
        public ActionResult Register()
        {
            var model = new RegisterNotificationsModel();
            model.JobDetails = _stuckWebService.GetCurrentJobDetails(GetToken());
            if (model.JobDetails != null && Session["bookingAddress"] != null)
            {
                var AddressGeoLocation = GoogleMapHelper.GetLatLong(Session["bookingAddress"].ToString());
                var availbleservices = _stuckWebService.GetServicesForLocation(Convert.ToString(AddressGeoLocation.latitude), Convert.ToString(AddressGeoLocation.longitude));
                model.Service = availbleservices.Where(service => service.serviceId == model.JobDetails.serviceId).FirstOrDefault();

            }
            model.UserProfile = _stuckWebService.GetCurrentCustomerDetails(GetToken());
            return View(model);
        }

        [HttpPost]
        public ActionResult Register(RegisterNotificationsModel model)
        {
            var jobDetails = _stuckWebService.GetCurrentJobDetails(GetToken());
            if (jobDetails != null)
            {
                var jobresult = _stuckWebService.ConfirmCurrentJob(jobDetails.version, jobDetails, GetToken());
                if (jobresult != null)
                {
                    // send email to user for job confirmation 
                    if (jobDetails != null && Session["bookingAddress"] != null)
                    {
                        var AddressGeoLocation = GoogleMapHelper.GetLatLong(Session["bookingAddress"].ToString());
                        var availbleservices = _stuckWebService.GetServicesForLocation(Convert.ToString(AddressGeoLocation.latitude), Convert.ToString(AddressGeoLocation.longitude));
                        model.Service = availbleservices.Where(service => service.serviceId == jobDetails.serviceId).FirstOrDefault();

                    }
                    var UserProfile = _stuckWebService.GetCurrentCustomerDetails(GetToken());
                    // create email body 
                    var mailManagement = new MailManagement();
                    string Body = string.Empty;
                    Body = Body + "<p> Dear " + UserProfile.name + ",</p>";
                    Body = Body + "<p>We have received your job request. Job Details are as mentioned below.</p>";

                    // table job description
                    Body = Body + "<table style='border:none;' cellspacing='10'>";
                    Body = Body + "<tr><td> Service Description </td><td> " + model.Service.serviceDescription + " </td></tr>";
                    string priceText = GetPriceText(model.Service.serviceId);
                    if (model.Service.isFixedCost)
                    {
                        Body = Body + "<tr><td> Service Fees </td><td> $" + model.Service.fixedPricingDetails.indicativeCost + " " + priceText + "</td></tr>";
                    }
                    else
                    {
                        Body = Body + "<tr><td> Service Fees </td><td> $" + model.Service.variablePricingDetails.basePrice + " " + priceText + "</td></tr>";
                    }

                    // lcoation
                    if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()))
                    {
                        Body = Body + "<tr><td> Start Location </td><td> " + Session["bookingAddress"].ToString() + " </td></tr>";
                    }
                    if (Session["destinationAddress"] != null && !string.IsNullOrEmpty(Session["destinationAddress"].ToString()))
                    {
                        Body = Body + "<tr><td> Destination Location </td><td> " + Session["destinationAddress"].ToString() + " </td></tr>";
                    }

                    // vechile details
                    Body = Body + "<tr><td> Vehicle Rego </td><td> " + jobDetails.vehicle.registration + " </td></tr>";
                    Body = Body + "<tr><td> Vehicle Year </td><td> " + jobDetails.vehicle.year + " </td></tr>";
                    Body = Body + "<tr><td> Vehicle Make </td><td> " + jobDetails.vehicle.make + " </td></tr>";
                    Body = Body + "<tr><td> Vehicle Model </td><td> " + jobDetails.vehicle.model + " </td></tr>";
                    Body = Body + "<tr><td> Vehicle Fuel Type </td><td> " + jobDetails.vehicle.fuelType + " </td></tr>";

                    // contact details
                    Body = Body + "<tr><td> Contact Phone # </td><td> " + UserProfile.phoneNumber + " </td></tr>";
                    Body = Body + "<tr><td> Contact Email </td><td> " + UserProfile.emailAddress + " </td></tr>";

                    Body = Body + "</table>";

                    Body = Body + "<p>Thank you for your job request. To track your Job progress please download the Stuck App and login.</p>";
                    Body = Body + "<p><br/>Thank You<br/>Stuck Team</p>";

                    var result = mailManagement.SendMail(UserProfile.emailAddress, "Stuck Web - Job Request Received", Body);

                    return RedirectToAction("StuckNotifications");
                }
            }
            return View();
        }

        private string GetPriceText(string serviceId)
        {
            return "only";
        }

        public ActionResult StuckNotifications(object model)
        {
            Session["ServiceId"] = string.Empty;
            Session["bookingAddress"] = string.Empty;
            Session["destinationAddress"] = string.Empty;
            Session["serviceRegion"] = string.Empty;
            TempData["BookingProgress"] = null;
            TempData["continueConfirmJob"] = null;

            var jobDetails = _stuckWebService.GetCurrentJobDetails(GetToken());
            if (Session["Notifications"] == null)
            {
                Session["Notifications"] = new Dictionary<string, string>();
            }
            var notifications = Session["Notifications"] as Dictionary<string, string>;
            if (jobDetails.jobStateId == "new")
            {
                if (!notifications.ContainsKey(jobDetails.jobId + "_" + jobDetails.jobStateId))
                {
                    notifications.Add(jobDetails.jobId + "_" + jobDetails.jobStateId, "<div style='background:gray;margin:5px;padding:1px;'><div style='color:white;margin:10px;'>We have recieved your job request.</div></div>");
                }
            }
            Session["Notifications"] = notifications;
            return View();
        }

        [HttpGet]
        public JsonResult GetCurrentJobState()
        {
            var notificationstring = "";
            var notificationcount = 0;
            NotificationsModel model = new NotificationsModel();
            try
            {
                var jobDetails = _stuckWebService.GetJobsHistory(GetToken())[0];
                if (jobDetails != null)
                {
                    if (Session["Notifications"] == null)
                    {
                        Session["Notifications"] = new Dictionary<string, string>();
                    }
                    var notifications = Session["Notifications"] as Dictionary<string, string>;
                    if (jobDetails.jobStateId == "pending_match")
                    {
                        if (!notifications.ContainsKey(jobDetails.jobId + "_" + jobDetails.jobStateId))
                        {
                            notifications.Add(jobDetails.jobId + "_" + jobDetails.jobStateId, "<div style='background:gray;margin:5px;padding:1px;'><div style='color:white;margin:10px;'>We are searching for sutiable supplier for your job request.</div></div>");
                        }
                    }
                    else if (jobDetails.jobStateId == "in_transit")
                    {
                        if (!notifications.ContainsKey(jobDetails.jobId + "_" + jobDetails.jobStateId))
                        {
                            SupplierDetails supplierDetails = _stuckWebService.GetSupplier(GetToken());
                            notifications.Add(jobDetails.jobId + "_" + jobDetails.jobStateId, "<div style='background:gray;margin:5px;padding:1px;'><div style='color:white;margin:10px;'>Supplier Matched and in Transit</div><div class='row' style='color:white;margin:20px;'><div class='col-md-4'>Supplier</div><div class='col-md-8'>" + supplierDetails.supplierName + "</div></div><div class='row' style='color:white;margin:20px;'><div class='col-md-4'>Organisation</div><div class='col-md-8'>" + supplierDetails.supplierOrganisation + "</div></div><div class='row' style='color:white;margin:20px;'><div class='col-md-4'>Phone #</div><div class='col-md-8'>" + supplierDetails.phoneNumber + "</div></div></div><div>");
                        }
                    }
                    else if (jobDetails.jobStateId == "cancelled")
                    {
                        if (!notifications.ContainsKey(jobDetails.jobId + "_" + jobDetails.jobStateId))
                        {
                            if (notifications.ContainsKey(jobDetails.jobId + "_in_transit"))
                            {
                                notifications.Add(jobDetails.jobId + "_" + jobDetails.jobStateId, "<div style='background:gray;margin:5px;padding:1px;'><div style='color:white;margin:10px;'>Your job is cancelled by supplier.</div></div>");
                            }
                            else
                            {
                                notifications.Add(jobDetails.jobId + "_" + jobDetails.jobStateId, "<div style='background:gray;margin:5px;padding:1px;'><div style='color:white;margin:10px;'>No mathing suppplier found, your job is cancelled.</div></div>");
                            }
                        }
                    }

                    Session["Notifications"] = notifications;
                    notificationcount = notifications.Count;

                    foreach (var item in notifications.Reverse())
                    {
                        notificationstring = notificationstring + item.Value;
                    }

                    model.Message = notificationstring;
                    model.Count = notificationcount;
                }
            }
            catch(Exception ex)
            {

            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSupplier()
        {
            _stuckWebService.GetSupplier(GetToken());
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }

}