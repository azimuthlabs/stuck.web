﻿using Auth0.AuthenticationApi;
using Auth0.AuthenticationApi.Models;
using Microsoft.Owin.Security;
using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Auth0.AspNet;
using System.Threading.Tasks;
using Stuck.API.Services;
using Stuck.BasePresentation.APIModels;
using Stuck.Services.Interfaces;
using Stuck.Domain;

namespace Stuck.Web.Presentation.Controllers
{
    public class AccountController : BaseController
    {
        readonly IBaseRepository<User> _userRepository;
        readonly IStuckWebServices _stuckWebService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController(IStuckWebServices stuckWebService, IBaseRepository<User> userRepository)
        {
            _stuckWebService = stuckWebService;
            _userRepository = userRepository;
        }

        [AllowAnonymous]
        public ActionResult Authentication(string returnUrl = "/Landing/LandingScreen")
        {
            if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()) && Session["serviceId"] != null && !string.IsNullOrEmpty(Session["serviceId"].ToString()))
            {
                returnUrl = "/Booking/BookingDetails";
            }

            var client = new AuthenticationApiClient(
            new Uri(string.Format("https://{0}", ConfigurationManager.AppSettings["auth0:Domain"])));
            var callbackurl = ConfigurationManager.AppSettings["callbackurl"].ToString();

            var request = this.Request;
            var redirectUri = new UriBuilder(request.Url.Scheme, request.Url.Host, this.Request.Url.IsDefaultPort ? -1 : request.Url.Port, callbackurl);

            var authorizeUrlBuilder = client.BuildAuthorizationUrl()
                .WithClient(ConfigurationManager.AppSettings["auth0:ClientId"])
                .WithRedirectUrl(redirectUri.ToString())
                .WithResponseType(AuthorizationResponseType.Code)
                .WithScope("openid profile")
                // adding this audience will cause Auth0 to use the OIDC-Conformant pipeline
                // you don't need it if your client is flagged as OIDC-Conformant (Advance Settings | OAuth)
                .WithAudience("https://" + @ConfigurationManager.AppSettings["auth0:Domain"] + "/userinfo");

            if (!string.IsNullOrEmpty(returnUrl))
            {
                var state = "ru=" + HttpUtility.UrlEncode(returnUrl);
                authorizeUrlBuilder.WithState(state);
            }

            return new RedirectResult(authorizeUrlBuilder.Build().ToString());
        }

        [HttpGet]
        public ActionResult Login(string returnUrl = "")
        {
            return View(new LoginModel());
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult OauthCallBack(string code)
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetAuthTokenDetails(string code = "")
        {

            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> Token(string code = "")
        {
            if (!string.IsNullOrEmpty(code))
            {
                AuthenticationApiClient client = new AuthenticationApiClient(
                    new Uri(string.Format("https://{0}", ConfigurationManager.AppSettings["auth0:Domain"])));

                var token = await client.GetTokenAsync(new AuthorizationCodeTokenRequest
                {
                    ClientId = ConfigurationManager.AppSettings["auth0:ClientId"],
                    ClientSecret = ConfigurationManager.AppSettings["auth0:ClientSecret"],
                    Code = code,
                    RedirectUri = Request.Url.Scheme + "://" + Request.Url.Authority +
    Request.ApplicationPath.TrimEnd('/') + "/"
                });

                Session["access_token"] = new AuthoTokenResponse
                {
                    access_token = token.AccessToken,
                    id_token = token.IdToken,
                    expires_in = token.ExpiresIn,
                    token_type = token.TokenType,

                };

                var profile = await client.GetUserInfoAsync(token.AccessToken);

                var user = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("name", profile.FullName ?? profile.PreferredUsername ?? profile.Email),
                    new KeyValuePair<string, object>("email", profile.Email),
                    new KeyValuePair<string, object>("family_name", profile.LastName),
                    new KeyValuePair<string, object>("given_name", profile.FirstName),
                    new KeyValuePair<string, object>("nickname", profile.NickName),
                    new KeyValuePair<string, object>("picture", profile.Picture),
                    new KeyValuePair<string, object>("user_id", profile.UserId),
                    new KeyValuePair<string, object>("id_token", token.IdToken),
                    new KeyValuePair<string, object>("access_token", token.AccessToken),
                    new KeyValuePair<string, object>("refresh_token", token.RefreshToken)
                };

                FederatedAuthentication.SessionAuthenticationModule.CreateSessionCookie(user);
                System.Web.Security.FormsAuthentication.SetAuthCookie(profile.FullName ?? profile.PreferredUsername ?? profile.Email, false);
                
                var dbUser = _userRepository.SearchFor(u => u.Username == profile.FullName || u.Email == profile.Email).FirstOrDefault();
                if (dbUser == null)
                {
                    TempData["BookingProgress"] = true;
                    return RedirectToAction("CustomerProfile");
                }

                if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()) && Session["serviceId"] != null && !string.IsNullOrEmpty(Session["serviceId"].ToString()))
                {
                    return RedirectToAction("BookingDetails", "Booking");
                }
                else
                {
                    return RedirectToAction("Location", "Landing");
                }
            }
            return Content("some error occured, please try again later.");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> GetAuthTokenDetails(AuthoTokenResponse model)
        {
            if (model != null && !string.IsNullOrEmpty(model.id_token))
            {
                Session["access_token"] = model;

                AuthenticationApiClient client = new AuthenticationApiClient(
                    new Uri(string.Format("https://{0}", ConfigurationManager.AppSettings["auth0:Domain"])));

                var profile = await client.GetUserInfoAsync(model.access_token);

                var user = new List<KeyValuePair<string, object>>
                {
                    new KeyValuePair<string, object>("name", profile.FullName ?? profile.PreferredUsername ?? profile.Email),
                    new KeyValuePair<string, object>("email", profile.Email),
                    new KeyValuePair<string, object>("family_name", profile.LastName),
                    new KeyValuePair<string, object>("given_name", profile.FirstName),
                    new KeyValuePair<string, object>("nickname", profile.NickName),
                    new KeyValuePair<string, object>("picture", profile.Picture),
                    new KeyValuePair<string, object>("user_id", profile.UserId),
                    new KeyValuePair<string, object>("id_token", model.id_token),
                    new KeyValuePair<string, object>("access_token", model.access_token),
                    // new KeyValuePair<string, object>("refresh_token", token.RefreshToken)
                };

                FederatedAuthentication.SessionAuthenticationModule.CreateSessionCookie(user);
                System.Web.Security.FormsAuthentication.SetAuthCookie(profile.FullName ?? profile.PreferredUsername ?? profile.Email, false);

                var dbUser = _userRepository.SearchFor(u => u.Username == profile.FullName || u.Email == profile.Email).FirstOrDefault();
                if (dbUser == null)
                {
                    TempData["BookingProgress"] = true;
                    return RedirectToAction("CustomerProfile");
                }

                if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()) && Session["serviceId"] != null && !string.IsNullOrEmpty(Session["serviceId"].ToString()))
                {
                    return RedirectToAction("BookingDetails", "Booking");
                }
                else
                {
                    return RedirectToAction("Location", "Landing");
                }

            }
            return View();
        }

        [HttpGet]
        public ActionResult CustomerProfile()
        {
            var profile = _stuckWebService.GetCurrentCustomerDetails(GetToken());
            var model = new UserModel
            {
                acceptedTermsAndConditions = profile.acceptedTermsAndConditions,
                emailAddress = profile.emailAddress,
                name = profile.name,
                phoneNumber = profile.phoneNumber,
                referralCode = profile.referralCode == null ? string.Empty : profile.referralCode.ToString(),
                version = profile.version
            };

            if (TempData["BookingProgress"] != null && Convert.ToBoolean(TempData["BookingProgress"]) == true)
                TempData.Keep("BookingProgress");

            if (TempData["continueConfirmJob"] != null && Convert.ToBoolean(TempData["continueConfirmJob"]) == true)
                TempData.Keep("continueConfirmJob");

            if (TempData["Success"] != null && Convert.ToBoolean(TempData["Success"]) == true)
                ViewBag.ShowSuccess = true;

            return View(model);
        }

        [HttpPost]
        public ActionResult CustomerProfile(UserModel model)
        {
            UserProfile profile = new UserProfile
            {
                acceptedTermsAndConditions = true,
                emailAddress = model.emailAddress,
                name = model.name,
                phoneNumber = model.phoneNumber,
                referralCode = model.referralCode == null ? null : model.referralCode.ToString()
            };
            var result = _stuckWebService.UpdateCustomerDetails(model.version, profile, GetToken());
            var dbUser = _userRepository.SearchFor(u => u.Username == model.emailAddress || u.Email == model.emailAddress).FirstOrDefault();
            if (dbUser == null)
            {
                Stuck.Domain.User user = new Domain.User();
                user.Email = model.emailAddress;
                user.Mobile = model.phoneNumber;
                user.Username = model.name;
                user.LastLogin = DateTime.Now;
                user.FirstLogin = true;
                user.UserId = Guid.NewGuid();
                _userRepository.Insert(user);
            }
            else
            {
                dbUser.Email = model.emailAddress;
                dbUser.Mobile = model.phoneNumber;
                dbUser.Username = model.name;
                dbUser.LastLogin = DateTime.Now;
                dbUser.FirstLogin = true;
                _userRepository.Update(dbUser, d => d.Email, d => d.Mobile, d => d.Username, d => d.LastLogin, d => d.FirstLogin);
            }

            if (TempData["continueConfirmJob"] != null && Convert.ToBoolean(TempData["continueConfirmJob"]) == true)
            {
                TempData["continueConfirmJob"] = null;
                return RedirectToAction("Register", "Notifications");
            }
            else if (TempData["BookingProgress"] != null && Convert.ToBoolean(TempData["BookingProgress"]) == true)
            {
                TempData["BookingProgress"] = null;
                return RedirectToAction("BookingDetails", "Booking");
            }
            else
            {
                TempData["Success"] = true;
                return RedirectToAction("CustomerProfile");
            }
        }

        public ActionResult Logout()
        {
            Session["access_token"] = null;
            Session["ServiceId"] = string.Empty;
            Session["bookingAddress"] = string.Empty;
            Session["destinationAddress"] = string.Empty;
            Session["serviceRegion"] = string.Empty;
            System.Web.Security.FormsAuthentication.SignOut();
            Response.Cookies.Clear();
            FederatedAuthentication.SessionAuthenticationModule.SignOut();
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Authentication", "Account");
        }
    }
}

