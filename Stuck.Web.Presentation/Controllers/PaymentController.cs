﻿using BasePresentation.Lookup;
using Braintree;
using Stuck.API.Services;
using Stuck.BasePresentation.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stuck.Web.Presentation.Controllers
{
    public class PaymentController : BaseController
    {
        readonly IStuckWebServices _stuckWebService;
        public API.Services.IBraintreeService _braintreeService = new API.Services.BraintreeService();

        public static readonly TransactionStatus[] transactionSuccessStatuses = {
                                                                                    TransactionStatus.AUTHORIZED,
                                                                                    TransactionStatus.AUTHORIZING,
                                                                                    TransactionStatus.SETTLED,
                                                                                    TransactionStatus.SETTLING,
                                                                                    TransactionStatus.SETTLEMENT_CONFIRMED,
                                                                                    TransactionStatus.SETTLEMENT_PENDING,
                                                                                    TransactionStatus.SUBMITTED_FOR_SETTLEMENT
                                                                                };

        public PaymentController(IStuckWebServices stuckWebService)
        {
            _stuckWebService = stuckWebService;
        }

        // GET: Payment
        public ActionResult Index()
        {
            var gateway = _braintreeService.GetGateway();
            var clientToken = gateway.ClientToken.generate();
            ViewBag.ClientToken = clientToken;
            return View();
        }

        public ActionResult Show(String id)
        {
            var gateway = _braintreeService.GetGateway();
            Transaction transaction = gateway.Transaction.Find(id);

            if (transactionSuccessStatuses.Contains(transaction.Status))
            {
                TempData["header"] = "Payment Success!";
                TempData["icon"] = "success";
                TempData["message"] = "Your transaction has been successfully processed.";
            }
            else
            {
                TempData["header"] = "Transaction Failed";
                TempData["icon"] = "fail";
                TempData["message"] = "Your transaction has a status of " + transaction.Status;
            };

            return View();
        }

        public ActionResult Create()
        {
            var nonce = Request["payment_method_nonce"];
            if (!string.IsNullOrEmpty(nonce))
            {
                BraintreePaymentDto braintreePaymentDto = new BraintreePaymentDto { PaymentNonce = nonce };
                var JobDetails = _stuckWebService.GetCurrentJobDetails(GetToken());
                var result = _stuckWebService.UpdatePaymentDetailsCurrentJob(braintreePaymentDto, GetToken(), JobDetails.version);
                if (result != null)
                {
                    return RedirectToAction("Register", "Notifications");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Flash"] = "Please enter valid payment details";
                return RedirectToAction("Index");
            }
        }

    }
}