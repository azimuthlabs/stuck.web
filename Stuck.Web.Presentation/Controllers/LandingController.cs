﻿using BasePresentation.Lookup;
using BasePresentation.Models;
using Stuck.API.Services;
using Stuck.BasePresentation.APIModels;
using Stuck.BasePresentation.Models;
using Stuck.Domain;
using Stuck.Services.Interfaces;
using Stuck.Web.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Stuck.Web.Presentation.Controllers
{
    [AllowAnonymous]
    public class LandingController : BaseController
    {
        readonly IBaseRepository<BlockedPostCode> _postcodeRepository;
        readonly IStuckWebServices _stuckWebService;

        public LandingController(IStuckWebServices stuckWebService, IBaseRepository<BlockedPostCode> postcodeRepository)
        {
            _stuckWebService = stuckWebService;
            _postcodeRepository = postcodeRepository;
        }

        [HttpGet]
        public ActionResult Location()
        {
            Session["ServiceId"] = string.Empty;
            Session["bookingAddress"] = string.Empty;
            Session["destinationAddress"] = string.Empty;
            Session["serviceRegion"] = string.Empty;
            TempData["BookingProgress"] = null;
            TempData["continueConfirmJob"] = null;

            // get all post codes
            var postCodes = _postcodeRepository.GetAll().Select(a => a.PostCode).ToList<double>();
            var blockedPostCodes = string.Join(",", postCodes);

            return View(new BookingModel { GoogleMapKey = Generic_Contsants.GoogleMapKey, BlockedPostCodes = blockedPostCodes });
        }

        [HttpPost]
        public ActionResult Location(BookingModel bookingModel)
        {
            Session["bookingAddress"] = bookingModel.Address;
            return RedirectToAction("LandingScreen");
        }

        public ActionResult Home()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult LandingScreen()
        {
            var model = new LandingPageViewModel { GoogleMapKey = Generic_Contsants.GoogleMapKey };
            if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()))
            {
                model.Address = Session["bookingAddress"].ToString();
            }

            // get all post codes
            var postCodes = _postcodeRepository.GetAll().Select(a => a.PostCode).ToList<double>();
            var blockedPostCodes = string.Join(",", postCodes);
            model.BlockedPostCodes = blockedPostCodes;

            return View(model);
        }

        [AllowAnonymous]
        public JsonResult GetServicesForLocation(string Latitude, string Longtitude)
        {
            return Json(_stuckWebService.GetServicesForLocation(Latitude, Longtitude), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Battery()
        {
            return View();
        }


        public ActionResult FlatTyre()
        {
            return View();
        }

        public ActionResult EmptyFuel()
        {
            return View();
        }

        public ActionResult LockedOut()
        {
            return View();
        }

        public ActionResult Mechanical()
        {
            return View();
        }

        public ActionResult Tow()
        {
            return View();
        }

        public ActionResult FAQ()
        {
            return View();
        }

        public ActionResult Media()
        {
            return View();
        }

        public ActionResult Latest()
        {
            return View();
        }

        public ActionResult Supplier()
        {
            return View();
        }

        public ActionResult Legal()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult Cookies()
        {
            return View();
        }

        public JsonResult SendEnquiryEmail(string Name, string Email, string Phone)
        {
            var ToEmail = ConfigurationManager.AppSettings["emailtosend"].ToString();
            var mailManagement = new MailManagement();
            string Body = string.Empty;
            Body = Body + "Hello Stuck Admin,<br/><br/>";
            Body = Body + "Enquiry is being received. Details are mentioned below<br/><br/>";
            Body = Body + "<b>Name : <b/>" + Name + "<br/><br/>";
            Body = Body + "<b>Email : <b/>" + Email + "<br/><br/>";
            Body = Body + "<b>Phone # : <b/>" + Phone + "<br/><br/>";
            Body = Body + "<b>Thank You,<b/><br/>";
            Body = Body + "Stuck Team";
            var result = mailManagement.SendMail(ToEmail, "Stuck Web - Enquiry Received", Body);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }

}