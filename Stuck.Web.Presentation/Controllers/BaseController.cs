﻿using Newtonsoft.Json;
using Stuck.API.Infrastructure;
using Stuck.API.Services;
using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Stuck.Web.Presentation.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        readonly string client_id = System.Configuration.ConfigurationManager.AppSettings["auth0:ClientId"].ToLower();
        readonly string client_secret = System.Configuration.ConfigurationManager.AppSettings["auth0:ClientSecret"].ToLower();
        readonly string client_domain = System.Configuration.ConfigurationManager.AppSettings["auth0:Domain"].ToLower();
        readonly IBaseStuckWebAPI _baseWebService;

        public BaseController()
        {
            _baseWebService = new BaseStuckWebAPI();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);  
        }

        public AuthoTokenResponse GetToken()
        {
            AuthoTokenResponse token = null;
            if (Session["access_token"] != null)
            {
                token = Session["access_token"] as AuthoTokenResponse;
            }
            return token;
        }
    }
}