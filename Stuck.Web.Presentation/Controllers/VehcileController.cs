﻿using BasePresentation.Lookup;
using Stuck.API.Services;
using Stuck.BasePresentation.APIModels;
using Stuck.Domain;
using Stuck.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stuck.Web.Presentation.Controllers
{

    public class VehcileController : BaseController
    {
        readonly IStuckWebServices _stuckWebService;
        readonly IBaseRepository<State> _stateRepository;

        public VehcileController(IStuckWebServices stuckWebService, IBaseRepository<State> stateRepository)
        {
            _stuckWebService = stuckWebService;
            _stateRepository = stateRepository;
        }

        public ActionResult VehcileDetails()
        {
            var Version = "2";
            if (TempData["Version"] != null && string.IsNullOrEmpty(TempData["Version"].ToString()))
                Version = TempData["Version"].ToString();
            var VechileResponseModel = _stuckWebService.GetVehcilesDetailsByVersion(Version, GetToken());
            if (VechileResponseModel == null) VechileResponseModel = new VechileResponseModel();
            if (VechileResponseModel.vehicle == null) VechileResponseModel.vehicle = new BasePresentation.APIModels.VehcileDetails();
            VechileResponseModel.States = _stateRepository.SearchFor(s => s.IsActive == true).ToList<State>();
            return View(VechileResponseModel);
        }

        public ActionResult VechileDetails(VechileResponseModel model)
        {
            GenericResponseModel response = _stuckWebService.CreateVehcilesDetails(model.jobVersion, model.vehicle, GetToken());
            if (response != null)
            {
                return RedirectToAction("Register", "Notifications");
            }

            return RedirectToAction("VehcileDetails", "Vehcile", new { Version = model.jobVersion });
        }

        public JsonResult SearchVechileDetails(string Rego, string State)
        {
            var vechilDetails = _stuckWebService.SearchVechilesDetails(Rego, State, GetToken());
            if (vechilDetails == null)
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            else
                return Json(vechilDetails, JsonRequestBehavior.AllowGet);
        }
    }
}