﻿using Auth0.AuthenticationApi.Models;
using BasePresentation.Lookup;
using Stuck.API.Services;
using Stuck.BasePresentation.Models;
using Stuck.Web.Presentation.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stuck.Web.Presentation.Controllers
{
    public class BookingController : BaseController
    {
        readonly IStuckWebServices _stuckWebService;
        public BookingController(IStuckWebServices stuckWebService)
        {
            _stuckWebService = stuckWebService;
        }
        
        [AllowAnonymous]
        public ActionResult ShowConfirmBooking(string Address, string ServiceId)
        {
            Session["bookingAddress"] = Address;
            Session["serviceId"] = ServiceId;
            return RedirectToAction("ConfirmBooking", "Booking");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ConfirmBooking()
        {
            BookingModel model = new BookingModel();
            model.GoogleMapKey = Generic_Contsants.GoogleMapKey;
            if (Session["ServiceId"] != null && !string.IsNullOrEmpty(Session["ServiceId"].ToString()))
            {
                model.ServiceId = Session["ServiceId"].ToString();
            }
            if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()))
            {
                model.Address = Session["bookingAddress"].ToString();
                if (!string.IsNullOrEmpty(model.Address))
                {
                    model.AddressGeoLocation = GoogleMapHelper.GetLatLong(model.Address);
                    var availbleservices = _stuckWebService.GetServicesForLocation(Convert.ToString(model.AddressGeoLocation.latitude), Convert.ToString(model.AddressGeoLocation.longitude));
                    model.Service = availbleservices.Where(service => service.serviceId == model.ServiceId).FirstOrDefault();
                    model.serviceRegion = model.Service.regionId;
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ConfirmBooking(BookingModel model)
        {
            if (!string.IsNullOrEmpty(model.ServiceId) && !string.IsNullOrEmpty(model.Address) && !string.IsNullOrEmpty(model.serviceRegion))
            {
                Session["ServiceId"] = model.ServiceId;
                Session["bookingAddress"] = model.Address;
                Session["serviceRegion"] = model.serviceRegion;
                if (!string.IsNullOrEmpty(model.Destination))
                {
                    Session["destinationAddress"] = model.Destination; // only for tow service;
                }
            }

            if (HttpContext.User.Identity.IsAuthenticated && !string.IsNullOrEmpty(HttpContext.User.Identity.Name) && Session["access_token"] != null)
            {
                return RedirectToAction("BookingDetails", "Booking");
            }
            else
            {
                return RedirectToAction("Authentication", "Account");
            }
        }

        public ActionResult BookingDetails()
        {
            JobModel model = new JobModel();
            if (Session["ServiceId"] != null && !string.IsNullOrEmpty(Session["ServiceId"].ToString()))
                model.serviceType = Session["ServiceId"].ToString();
            if (Session["bookingAddress"] != null && !string.IsNullOrEmpty(Session["bookingAddress"].ToString()))
                model.startLocation = GoogleMapHelper.GetLatLong(Session["bookingAddress"].ToString());
            if (Session["serviceRegion"] != null && !string.IsNullOrEmpty(Session["serviceRegion"].ToString()))
                model.serviceRegion = Session["serviceRegion"].ToString();
            model.endLocation = new LatLongModel { latitude = 0, longitude = 0 };
            var response =_stuckWebService.CreateJob(model, GetToken());
            // give destination for tow job and update on api
            if(response != null && response.id != Guid.Empty)
            {
                if (Session["destinationAddress"] != null && !string.IsNullOrEmpty(Session["destinationAddress"].ToString()))
                {
                    LatLongModel endLocation = GoogleMapHelper.GetLatLong(Session["destinationAddress"].ToString());
                    var destinationresponse = _stuckWebService.CreateJobDestinationDetailsTowJob(endLocation, GetToken(), response.version);
                    if(destinationresponse != null)
                    {
                        TempData["Version"] = destinationresponse.version;
                        return RedirectToAction("VehcileDetails", "Vehcile");
                    }
                    else
                    {
                        return RedirectToAction("JobCreationError", "Error");
                    }
                }
                else
                {
                    TempData["Version"] = response.version;
                    return RedirectToAction("VehcileDetails", "Vehcile");
                }
            }
            else
            {
                return RedirectToAction("JobCreationError", "Error");
            }
        }

        public ActionResult Bookings()
        {
            return View();
        }
    }
}