﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.Services.Interfaces
{
    public interface IBaseRepository<T>
    {
        void Insert(T entity);
        void Update(T obj, params Expression<Func<T, object>>[] propertiesToUpdate);
        void Delete(T entity);
        IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate);
        IQueryable<T> GetAll();
        T GetById(int id);
    }
}
