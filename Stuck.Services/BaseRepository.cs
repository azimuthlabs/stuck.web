﻿using Stuck.Domain;
using Stuck.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.Services
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private stuckdevEntities _dbContext = new stuckdevEntities();
        

        public BaseRepository()
        {
            //_dbContext = dbContext;
        }

        public void Insert(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            _dbContext.SaveChanges();
        }

        public void Update(TEntity obj, params Expression<Func<TEntity, object>>[] propertiesToUpdate)
        {
            _dbContext.Set<TEntity>().Attach(obj);

            foreach (var p in propertiesToUpdate)
            {
                _dbContext.Entry(obj).Property(p).IsModified = true;
            }

            _dbContext.SaveChanges();
        }


        public void Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().Where(predicate);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return _dbContext.Set<TEntity>().Find(id);
        }

    }
}
