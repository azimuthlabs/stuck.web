﻿using Stuck.API.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasePresentation.Lookup;
using System.Net.Http;
using Stuck.BasePresentation.APIModels;
using Newtonsoft.Json;
using System.IO;
using Stuck.BasePresentation.Models;

namespace Stuck.API.Services
{
    public class StuckWebServices : BaseStuckWebAPI, IStuckWebServices
    {
        // Get Available services and pricing for location parameter latitude and longitude
        public List<ServiceDetails> GetServicesForLocation(string Latitude, string Longtitude)
        {
            List<ServiceDetails> serviceData = new List<ServiceDetails>();
            var services = new List<ServiceDetails>();
            try
            {
                HttpResponseMessage response = InvokeAPI(StuckAPI_Constant.ServicesAndPricingAPI + "latitude=" + Latitude + "&longitude=" + Longtitude);
                var data = JsonConvert.DeserializeObject<List<ServiceDetails>>(response.Content.ReadAsStringAsync().Result.ToString());

                // changes as per feedback from client on 07022018
                if (data != null && data.Count > 1)
                {
                    var battery = data.Where(d => d.serviceId == ServiceType_Constant.FlatBattery).FirstOrDefault();
                    if (battery != null)
                        services.Add(battery);

                    var tyre = data.Where(d => d.serviceId == ServiceType_Constant.FlatTyre).FirstOrDefault();
                    if (tyre != null)
                        services.Add(tyre);

                    var fuel = data.Where(d => d.serviceId == ServiceType_Constant.NoFuel).FirstOrDefault();
                    if (fuel != null)
                    {
                        fuel.serviceDescription = "Out of Fuel";
                        services.Add(fuel);
                    }

                    var lockout = data.Where(d => d.serviceId == ServiceType_Constant.LockedOut).FirstOrDefault();
                    if (lockout != null)
                        services.Add(lockout);

                    var mechanical = data.Where(d => d.serviceId == ServiceType_Constant.Mechanical).FirstOrDefault();
                    if (mechanical != null)
                        services.Add(mechanical);

                    var tow = data.Where(d => d.serviceId == ServiceType_Constant.Tow).FirstOrDefault();
                    if (tow != null)
                        services.Add(tow);
                }

            }
            catch (Exception ex)
            {
                services = null;
            }
            return services;
        }

        // Create a new Job 
        public JobCreationResponseDetails CreateJob(JobModel data, AuthoTokenResponse token)
        {
            JobCreationResponseDetails jobresponse = null;
            try
            {
                HttpResponseMessage response = InvokePostAPI(StuckAPI_Constant.CreateJob, data, token);
                var result = response.Content.ReadAsStringAsync().Result.ToString();
                if (!string.IsNullOrEmpty(result))
                    jobresponse = JsonConvert.DeserializeObject<JobCreationResponseDetails>(result);
            }
            catch (Exception ex)
            {
                //return null;
            }
            return jobresponse;
        }

        public JobCreationResponseDetails CreateJobDestinationDetailsTowJob(LatLongModel data, AuthoTokenResponse token, string version)
        {
            JobCreationResponseDetails jobresponse = null;
            try
            {
                HttpResponseMessage response = InvokePutAPI(string.Format(StuckAPI_Constant.CurrentJobDestination, version), data, token);
                var result = response.Content.ReadAsStringAsync().Result.ToString();
                if (!string.IsNullOrEmpty(result))
                {
                    jobresponse = JsonConvert.DeserializeObject<JobCreationResponseDetails>(result);
                }
            }
            catch (Exception ex)
            {
            }
            return jobresponse;
        }

        public GenericResponseModel CreateVehcilesDetails(string Version, VehcileDetails Data, AuthoTokenResponse token)
        {
            GenericResponseModel vehcileDetails = null;
            try
            {
                HttpResponseMessage response = InvokePutAPI(string.Format(StuckAPI_Constant.GetVehiclesDetailsByVersion, Version), Data, token);
                vehcileDetails = JsonConvert.DeserializeObject<GenericResponseModel>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
            }
            return vehcileDetails;
        }

        public VechileResponseModel GetVehcilesDetailsByVersion(string Version, AuthoTokenResponse token)
        {
            VechileResponseModel vehcileDetails = null;
            try
            {
                HttpResponseMessage response = InvokeAPI(string.Format(StuckAPI_Constant.GetVehiclesDetailsByVersion, Version), token);
                vehcileDetails = JsonConvert.DeserializeObject<VechileResponseModel>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
            }
            return vehcileDetails;
        }

        public VehcileDetails SearchVechilesDetails(string licencePlate, string state, AuthoTokenResponse token)
        {
            try
            {
                // token.id_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfbWV0YWRhdGEiOnsic2lnbmVkX3VwIjp0cnVlLCJ1c2VybmFtZSI6bnVsbCwiY3VzdG9tZXJfaWQiOiJjMzc4YmQ4MC0xNzA3LTExZTgtOTQyZi00MTI0ZjQxYmI1NTUifSwiY3VzdG9tZXJfaWQiOiJjMzc4YmQ4MC0xNzA3LTExZTgtOTQyZi00MTI0ZjQxYmI1NTUiLCJpc3MiOiJodHRwczovL2N1c3RvbWVyLXByb2R1Y3Rpb24uYXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhOGQ2YmRjYjczNTdiNDY0NDVjNGYxMiIsImF1ZCI6IlpwMGk4OUFQUEFldDFaNjhmcDNtSXJUT01JWnJrbHpvIiwiaWF0IjoxNTE5MjE4Mjc1LCJleHAiOjE1MTkyNTQyNzV9.RHPUI5PjG0ybZBBqoqggbMSaEW1LBr3c-zo6zdbWuGM";
                // HttpResponseMessage response = InvokeAPI(string.Format("https://stuckcustomer-public-api-prod.azimuthlabs.com.au/api/v1/vehicles?licencePlate={0}&state={1}", licencePlate, state), token);
                HttpResponseMessage response = InvokeAPI(string.Format(StuckAPI_Constant.SearchVehiclesDetails,licencePlate, state), token);
                var vehcileDetails = JsonConvert.DeserializeObject<VehcileDetails>(response.Content.ReadAsStringAsync().Result.ToString());
                return vehcileDetails;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetPaymentToken(AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokePutAPI(StuckAPI_Constant.GetPaymentToken, null, token);
                return JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public UserProfile GetCurrentCustomerDetails(AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokeAPI(StuckAPI_Constant.CustomerDetails, token);
                return JsonConvert.DeserializeObject<UserProfile>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return new UserProfile();
            }
        }

        public GenericResponseModel UpdateCustomerDetails(int version, UserProfile Data, AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokePutAPI(string.Format(StuckAPI_Constant.UpdateCustomerDetails, version), Data, token);
                return JsonConvert.DeserializeObject<GenericResponseModel>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public JobDetails GetCurrentJobDetails(AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokeAPI(StuckAPI_Constant.CurrentJob, token);
                return JsonConvert.DeserializeObject<JobDetails>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public JobDetails ConfirmCurrentJob(int Version, JobDetails Data, AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokePostAPI(string.Format(StuckAPI_Constant.ConfirmAJob, Version), Data, token);
                return JsonConvert.DeserializeObject<JobDetails>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public GenericResponseModel RegisterJobNotifications(CustomerRegistration Data, AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokePutAPI(string.Format(StuckAPI_Constant.RegisterForNotifications), Data, token);
                return JsonConvert.DeserializeObject<GenericResponseModel>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public JobDetails CurrentJobState(AuthoTokenResponse token)
        {
            try
            {
                HttpResponseMessage response = InvokeAPI(StuckAPI_Constant.CurrentJobState, token);
                return JsonConvert.DeserializeObject<JobDetails>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public GenericResponseModel UpdatePaymentDetailsCurrentJob(BraintreePaymentDto PaymentNonce, AuthoTokenResponse token, int Version)
        {
            try
            {
                HttpResponseMessage response = InvokePutAPI(string.Format(StuckAPI_Constant.UpdatePaymentDetails, Version), PaymentNonce, token);
                return JsonConvert.DeserializeObject<GenericResponseModel>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public SupplierDetails GetSupplier(AuthoTokenResponse token)
        {
            SupplierDetails supplierDetails = null;
            try
            {
                HttpResponseMessage response = InvokeAPI(string.Format(StuckAPI_Constant.CurrentSupplier), token);
                supplierDetails = JsonConvert.DeserializeObject<SupplierDetails>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
            }
            return supplierDetails;
        }

        public List<JobDetails> GetJobsHistory(AuthoTokenResponse token)
        {
            List<JobDetails> JobsHistory = null;
            try
            {
                HttpResponseMessage response = InvokeAPI(string.Format(StuckAPI_Constant.JobsHistory), token);
                JobsHistory = JsonConvert.DeserializeObject<List<JobDetails>>(response.Content.ReadAsStringAsync().Result.ToString());
            }
            catch (Exception ex)
            {
            }
            return JobsHistory;
        }
    }
}
