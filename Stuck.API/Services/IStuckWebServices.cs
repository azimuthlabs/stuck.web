﻿using Stuck.BasePresentation.APIModels;
using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.API.Services
{
    public interface IStuckWebServices
    {
        List<ServiceDetails> GetServicesForLocation(string Latitude, string Longtitude);
        JobCreationResponseDetails CreateJob(JobModel data, AuthoTokenResponse token);
        JobDetails GetCurrentJobDetails(AuthoTokenResponse token);
        JobDetails ConfirmCurrentJob(int Version, JobDetails Data, AuthoTokenResponse token);
        JobDetails CurrentJobState(AuthoTokenResponse token);
        JobCreationResponseDetails CreateJobDestinationDetailsTowJob(LatLongModel data, AuthoTokenResponse token, string version);
        GenericResponseModel CreateVehcilesDetails(string Version, VehcileDetails Data, AuthoTokenResponse token);
        VehcileDetails SearchVechilesDetails(string licencePlate, string state, AuthoTokenResponse token);
        VechileResponseModel GetVehcilesDetailsByVersion(string Version, AuthoTokenResponse token);
        string GetPaymentToken(AuthoTokenResponse token);
        UserProfile GetCurrentCustomerDetails(AuthoTokenResponse token);
        GenericResponseModel UpdateCustomerDetails(int version, UserProfile Data, AuthoTokenResponse token);
        GenericResponseModel RegisterJobNotifications(CustomerRegistration Data, AuthoTokenResponse token);
        GenericResponseModel UpdatePaymentDetailsCurrentJob(BraintreePaymentDto PaymentNonce, AuthoTokenResponse token, int Version);
        SupplierDetails GetSupplier(AuthoTokenResponse token);
        List<JobDetails> GetJobsHistory(AuthoTokenResponse token);
    }
}
