﻿using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.API.Infrastructure
{
    public interface IBaseStuckWebAPI
    {
        HttpResponseMessage InvokeAPI(string EndPoint, AuthoTokenResponse AuthorizationToken = null);
        HttpResponseMessage InvokePostAPI(string EndPoint, object Data, AuthoTokenResponse AuthorizationToken = null);
        HttpResponseMessage InvokePutAPI(string EndPoint, object Data, AuthoTokenResponse AuthorizationToken = null);
    }
}
