﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.API.Infrastructure
{
    public class BaseStuckWebAPI : IBaseStuckWebAPI
    {
        public HttpResponseMessage InvokeAPI(string EndPoint, AuthoTokenResponse AuthorizationToken = null)
        {
            HttpClient client = new HttpClient();
            if (AuthorizationToken != null)
            {
                string _ContentType = "application/json";
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthorizationToken.id_token);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(_ContentType));
            }
            HttpResponseMessage response = client.GetAsync(EndPoint).GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            return null;
        }

        public HttpResponseMessage InvokePostAPI(string EndPoint, object Data, AuthoTokenResponse AuthorizationToken)
        {
            HttpClient client = new HttpClient();
            string _ContentType = "application/json";
            if (AuthorizationToken != null)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthorizationToken.id_token);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(_ContentType));
            }
            var stringContent = new StringContent(JsonConvert.SerializeObject(Data), Encoding.UTF8, _ContentType);
            HttpResponseMessage response = client.PostAsync(EndPoint, stringContent).GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            return null;
        }

        public HttpResponseMessage InvokePutAPI(string EndPoint, object Data, AuthoTokenResponse AuthorizationToken = null)
        {
            HttpClient client = new HttpClient();
            string _ContentType = "application/json";
            if (AuthorizationToken != null)
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AuthorizationToken.id_token);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(_ContentType));
            }
            HttpResponseMessage response = null;
            if (Data != null)
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(Data), Encoding.UTF8, _ContentType);
                response = client.PutAsync(EndPoint, stringContent).GetAwaiter().GetResult();
            }
            else
            {
                response = client.PutAsync(EndPoint, null).GetAwaiter().GetResult();
            }
            if (response.IsSuccessStatusCode)
            {
                return response;
            }
            return null;
        }
    }

}
