﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class NotificationsModel
    {
        public string Message { get; set; }
        public int Count { get; set; }
    }
}
