﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class UserModel
    {
        public int version { get; set; }
        public object payment { get; set; }
        [Required (ErrorMessage = "Name can not be blank")]
        public string name { get; set; }
        [Required (ErrorMessage = "Email can not be blank")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.\']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,8}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid Email")]
        public string emailAddress { get; set; }
        [Required (ErrorMessage = "Phone Number can not be blank")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone Number is Invalid")]
        public string phoneNumber { get; set; }
        public string referralCode { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "You need to Accept Terms and Conditions")]
        public bool acceptedTermsAndConditions { get; set; }
    }
}
