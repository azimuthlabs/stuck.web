﻿using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasePresentation.Models
{
    public class LandingPageViewModel : BaseModel
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public string BlockedPostCodes { get; set; }
    }
}
