﻿using Stuck.BasePresentation.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class BookingModel : BaseModel
    {
        public string Address { get; set; }
        public string Destination { get; set; }
        public double DestinationLatitude { get; set; }
        public double DestinationLongtitude { get; set; }
        public string ServiceType { get; set; }
        public string ServiceId { get; set; }
        public string serviceRegion { get; set; }

        public LatLongModel AddressGeoLocation { get; set; }
        public LatLongModel DestinationGeoLocation { get; set; }
        public ServiceDetails Service { get; set; }
        public string BlockedPostCodes { get; set; }
    }
}
