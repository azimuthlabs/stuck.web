﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class JobModel
    {
        public string serviceType { get; set; }
        public string serviceRegion { get; set; }
        public LatLongModel startLocation { get; set; }
        public LatLongModel endLocation { get; set; }
    }
}
