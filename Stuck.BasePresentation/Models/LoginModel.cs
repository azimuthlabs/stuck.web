﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username is required")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Username should contain minimum 5 and maximum 30 characters")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Password should contain minimum 5 and maximum 30 characters")]
        public string Password { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Username is required")]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Username should contain minimum 5 and maximum 30 characters")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Confirm - Password is required")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConfirmPasswrod { get; set; }
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Password should contain minimum 5 and maximum 30 characters")]
        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
