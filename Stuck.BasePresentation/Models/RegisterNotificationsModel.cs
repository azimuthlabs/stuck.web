﻿using Stuck.BasePresentation.APIModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.Models
{
    public class RegisterNotificationsModel
    {
        public bool Register { get; set; }
        public JobDetails JobDetails { get; set; }
        public UserProfile UserProfile { get; set; } 
        public ServiceDetails Service { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "You need to Accept Terms and Conditions")]
        public bool acceptedTermsAndConditions { get; set; }
    }
}
