﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace BasePresentation.Lookup
{
    public class Generic_Contsants
    {
        // public static string GoogleMapKey = "AIzaSyBXtILjuIo_7IH2LnuXotns0XqYrbDZ89g"; // Registered By Sachin
        // public static string GoogleMapKey = "AIzaSyBuBqLPzkWfGfWvIu1f_wjhLUVDLCQ6Nm8"; // Registered By Harish
        public static string GoogleMapKey = "AIzaSyBS4nIn1rNjzO7SWKjUnLOPQPNsBCz6YnQ"; // used in Mobile APP Old Team
    }

    public class StuckAPI_Constant
    {
        public static string ServiceBaseUrl = ConfigurationManager.AppSettings["ServiceBaseUrl"].ToString();
        public static string ServicesAndPricingAPI = ServiceBaseUrl + "api/v1/servicetypes?";
        public static string CreateJob = ServiceBaseUrl + "api/v1/jobs";
        public static string CurrentJob = ServiceBaseUrl + "api/v1/jobs/current";
        public static string ConfirmAJob = ServiceBaseUrl + "api/v1/jobs/current/confirm?version={0}";
        public static string CurrentJobState = ServiceBaseUrl + "api/v1/jobs/current/state";
        public static string CurrentJobDestination = ServiceBaseUrl + "api/v1/jobs/current/destination?version={0}";
        public static string SearchVehiclesDetails = ServiceBaseUrl + "api/v1/vehicles?licencePlate={0}&state={1}";
        public static string GetVehiclesDetailsByVersion = ServiceBaseUrl + "api/v1/jobs/current/vehicle?version={0}";
        public static string CreateVehcilesDetails = ServiceBaseUrl + "api/v1/jobs/current/vehicle?version={0}";
        public static string GetPaymentToken = ServiceBaseUrl + "api/v1/customer/current/payment/token";
        public static string UpdatePaymentDetails = ServiceBaseUrl + "api/v1/jobs/current/payment-nonce?version={0}";
        public static string CustomerDetails = ServiceBaseUrl + "api/v1/customer/current";
        public static string UpdateCustomerDetails = ServiceBaseUrl + "api/v1/customer/current/details?version={0}";
        public static string RegisterForNotifications = ServiceBaseUrl + "api/v1/jobs/notifications/register";
        public static string CurrentSupplier = ServiceBaseUrl + "api/v1/jobs/current/supplier";
        public static string JobsHistory = ServiceBaseUrl + "api/v1/jobs/history";

    }

    public class ServiceType_Constant
    {
        public static string LockedOut = "locked-out";
        public static string Tow = "tow";
        public static string FlatBattery = "flat-battery";
        public static string FlatTyre = "flat-tyre";
        public static string Mechanical = "mechanical";
        public static string NoFuel = "no-fuel";
    }

    public class PaymentType_Constant
    {
        public static string CC = "creditcard";
        public static string Paypal = "paypal";
        public static string PaymentServiceId = "braintree";
    }
}
