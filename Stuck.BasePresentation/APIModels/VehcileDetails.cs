﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class VehcileDetails
    {
        [Required (ErrorMessage ="Vechile Make is requied")]
        public string make { get; set; }
        [Required(ErrorMessage = "Vechile Model is requied")]
        public string model { get; set; }
        [Required(ErrorMessage = "Vechile Year is requied")]
        [Range(1980, 2018, ErrorMessage = "Year is not valid")]
        public int? year { get; set; }
        [Required(ErrorMessage = "Vechile Fuel Type is requied")]
        public string fuelType { get; set; }
        [Required(ErrorMessage = "Vechile Registration is requied")]
        public string registration { get; set; }

        public int CurrentYear
        {
            get { return DateTime.Now.Year; }
        }
    }
}
