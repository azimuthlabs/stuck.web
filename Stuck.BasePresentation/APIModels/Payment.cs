﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class Payment
    {
        public string paymentMethod { get; set; }
        public string paymentDescription { get; set; }
        public string externalIdentifier { get; set; }
        public string preauthTransactionId { get; set; }
        public string transactionId { get; set; }
    }

}
