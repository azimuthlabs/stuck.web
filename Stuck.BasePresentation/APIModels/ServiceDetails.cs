﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class ServiceDetails
    {
        public string id { get; set; }
        public string serviceId { get; set; }
        public string serviceDescription { get; set; }
        public string serviceIconUrl { get; set; }
        public string invertedServiceIconUrl { get; set; }
        public bool active { get; set; }
        public string regionId { get; set; }
        public string regionTimezone { get; set; }
        public int pricingVersion { get; set; }
        public bool isFixedCost { get; set; }
        public bool isVariableCost { get; set; }
        public FixedPricingDetails fixedPricingDetails { get; set; }
        public VariablePricingDetails variablePricingDetails { get; set; }
        public string periodId { get; set; }
        public double? matchingDistance { get; set; }

    }
}
