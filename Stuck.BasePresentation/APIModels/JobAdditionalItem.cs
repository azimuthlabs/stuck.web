﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class JobAdditionalItem
    {
        public string description { get; set; }
        public int quantity { get; set; }
        public int unitPrice { get; set; }
    }
}
