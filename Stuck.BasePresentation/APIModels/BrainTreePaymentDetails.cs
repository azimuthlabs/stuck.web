﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class BraintreePaymentDto
    {
        public string PaymentNonce { get; set; }
    }
}
