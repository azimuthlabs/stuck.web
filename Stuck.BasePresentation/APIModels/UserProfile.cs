﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class UserProfile
    {
        public string id { get; set; }
        public int version { get; set; }
        public object payment { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string phoneNumber { get; set; }
        public object referralCode { get; set; }
        public bool acceptedTermsAndConditions { get; set; }
    }
}
