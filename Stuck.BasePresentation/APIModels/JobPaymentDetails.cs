﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class JobPaymentDetails
    {
        public string paymentMethodId { get; set; }
        public string paymentServiceId { get; set; }
        public string paymentDescription { get; set; }
    }
}
