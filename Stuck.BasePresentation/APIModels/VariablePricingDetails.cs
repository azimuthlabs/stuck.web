﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class VariablePricingDetails
    {
        public double basePrice { get; set; }
        public double includedUnits { get; set; }
        public string units { get; set; }
        public double additionalPricePerUnitAboveBase { get; set; }
        public string id { get; set; }
        public DateTime updatedAt { get; set; }
        public int version { get; set; }
        public double earlyCancellationFee { get; set; }
        public double inTransitCancellationFee { get; set; }
        public double serviceUnderwayCancellationFee { get; set; }

    }
}
