﻿using Stuck.BasePresentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class JobDetails
    {
        public string jobId { get; set; }
        public int version { get; set; }
        public string serviceId { get; set; }
        public string serviceRegionId { get; set; }
        public string serviceRegionPeriodId { get; set; }
        public LatLongModel StartLocation { get; set; }
        public LatLongModel EndLocation { get; set; }
        public LatLongModel ServiceCompleteLocation { get; set; }
        public string customerId { get; set; }
        public string referralCode { get; set; }
        public string supplierId { get; set; }
        public string organisationId { get; set; }
        public VehcileDetails vehicle { get; set; }
        public bool isActive { get; set; }
        public bool isCancelled { get; set; }
        public bool hasCustomerRated { get; set; }
        public bool hasSupplierRated { get; set; }
        public Payment payment { get; set; }
        public string jobStateId { get; set; }
        public bool isWatchOverMeActive { get; set; }
        public List<JobAdditionalItem> jobAdditionalItems { get; set; }
    }
}
