﻿using Stuck.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class VechileResponseModel
    {
        public Guid jobId { get; set; }
        public string jobVersion { get; set; }
        public VehcileDetails vehicle { get; set; }
        public IEnumerable<State> States { get; set; }
    }
}
