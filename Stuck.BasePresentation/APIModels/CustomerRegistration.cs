﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class CustomerRegistration
    {
        public string Handle { get; set; }
        public string Platform { get; set; }
        public string Jobid { get; set; }
    }
}
