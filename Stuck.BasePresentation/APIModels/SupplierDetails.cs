﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuck.BasePresentation.APIModels
{
    public class SupplierDetails
    {
        public string supplierId { get; set; }
        public int version { get; set; }
        public string organisationId { get; set; }
        public string supplierStateId { get; set; }
        public bool isActive { get; set; }
        public string supplierName { get; set; }
        public string supplierOrganisation { get; set; }
        public string organisationEmail { get; set; }
        public object vehicleRegistration { get; set; }
        public object vehicleDetails { get; set; }
        public string phoneNumber { get; set; }
        public string supplierPhotoUrl { get; set; }
        public List<ActiveService> activeServices { get; set; }
        public List<AssignedService> assignedServices { get; set; }

    }

    public class ActiveService
    {
        public string serviceId { get; set; }
        public string serviceName { get; set; }
        public double rating { get; set; }
    }

    public class AssignedService
    {
        public string serviceId { get; set; }
        public string serviceName { get; set; }
        public double rating { get; set; }
    }

}
